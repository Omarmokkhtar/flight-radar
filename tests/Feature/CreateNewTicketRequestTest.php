<?php

namespace Tests\Feature;

use Tests\TestCase;
use Faker\Factory as Faker;

class CreateNewTicketRequestTest extends TestCase
{

    public function test_create_ticket_returns_validation_error_response(): void
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/tickets');
        $response->assertStatus(422);
    }

    public function test_create_ticket_returns_success_response(): void
    {
        $faker = Faker::create();

        $data = [
            'departure_date' => $faker->dateTime()->format('Y-m-d H:i'),
            'source' => $faker->country,
            'destination' => $faker->country,
            'passport_id' => 'A212344'
        ];
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/tickets', $data);
        $response->assertStatus(200);
    }

}
