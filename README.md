# Installation

- Clone the repo
- copy .env.example and rename it to .env
- Add database name and username and password in the .env file
- Run ``composer install ``
- Run the migration using ``php artisan:migrate ``

## Usage
- Attached in the email Postman collection for the APIs
- Run ``php artisan serv`` to start the project locally
- To run the event on the database use ``php artisan schedule:run``
- To run unit test use `` php artisan test ``

# Technical details
- So the project was done using the service-repo design pattern where you will be able to see each part of the code separated on its own to be able to do maintenance easily in the future
- Added 2 Feature tests to the create ticket API
- Added event on the tickets table to check if the ticket created is confirmed or not after 3 minutes and you can execute the event locally using the previously mentioned command 
- Added Validation layer to Validate the data separately 
