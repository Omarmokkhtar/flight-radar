<?php

namespace App\Repositories;

use App\Models\Ticket;
use Illuminate\Support\Facades\Crypt;

class TicketRepository{

    protected Ticket $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }


    public function storeDataRepository($data): array
    {
        $this->ticket->departure_date = $data['departure_date'];
        $this->ticket->source = $data['source'];
        $this->ticket->destination = $data['destination'];
        $this->ticket->passport_number = Crypt::encryptString($data['passport_id']);
        $this->ticket->seat_number = $data['seat_number'];
        if (isset($data['status']) && $data['status'] == 0) {
            $this->ticket->status = $data['status'];
        }
        $this->ticket->save();
        return [
            'ticket_id' => $this->ticket->id,
            'message' => isset($data['status']) ? 'You have three minutes to confirm the ticket or it will be deleted' : 'Ticket Created'
        ];
    }

    public function checkRandomNumber($data): array|int
    {

        $ticketNumbers = Ticket::select('seat_number')
            ->where('source', $data['source'])
            ->where('destination', $data['destination'])
            ->get()->toArray();

        if (count($ticketNumbers) == 32){
            return ['error' => 'Airplane already full' , 'status' =>409];
        }
        $randomNumber = rand(1, 32);

        while (in_array($randomNumber, $ticketNumbers)) {
            $randomNumber = rand(1, 32);
        }

        return $randomNumber;
    }

    public function deleteTicketRepository($id)
    {
        $ticket = $this->ticket->find($id);
        if (!$ticket) {
            return ['error' => 'Item not found' , 'status' =>404];
        }
        return $ticket->delete();

    }

    public function updateTicketRepository($data, $id)
    {
        $ticket = Ticket::find($id);
        if (!$ticket) {
            return ['error' => 'Item not found' , 'status' =>404];
        }
        if (!empty($this->checkSeatNumber($ticket, $data['seat_number'])))
        {
            return ['error' => 'Seat number already reserved' , 'status' =>409];

        }
        if (empty($this->checkSeatNumber($ticket, $data['seat_number'])))
        {
            $ticket->seat_number = $data['seat_number'];
            return $ticket->update();
        }
    }

    private function checkSeatNumber($ticketData , $seatNumber)
    {
        return Ticket::select('id')
            ->where('source', $ticketData['source'])
            ->where('destination', $ticketData['destination'])
            ->where('seat_number', $seatNumber)
            ->get()->toArray();
    }

}
