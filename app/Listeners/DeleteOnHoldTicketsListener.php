<?php

namespace App\Listeners;

use App\Events\DeleteOnHoldTickets;
use Illuminate\Support\Facades\DB;

class DeleteOnHoldTicketsListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(DeleteOnHoldTickets $event): void
    {
        DB::table('tickets')->where('status', '=',0)->delete();
    }
}
