<?php

namespace App\Services;

use App\Repositories\TicketRepository;

class TicketService{


    protected TicketRepository $ticketRepository;

    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }


    public function storeTicketService($data): array
    {

        $data['seat_number'] = $this->checkSeatNumber($data);
        if (!is_numeric($data['seat_number'])){
            return $data['seat_number'];
        }
        return $this->ticketRepository->storeDataRepository($data);
    }

    protected function checkSeatNumber($data): int|array
    {
        return $this->ticketRepository->checkRandomNumber($data);
    }

    public function deleteTicketService($id)
    {
        return $this->ticketRepository->deleteTicketRepository($id);
    }

    public function updateTicketService($data, $id)
    {
        return $this->ticketRepository->updateTicketRepository($data, $id);

    }
}
