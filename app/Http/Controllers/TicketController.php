<?php

namespace App\Http\Controllers;

use PHPUnit\Exception;
use App\Services\TicketService;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StoreTicketRequest;
use App\Http\Requests\UpdateTicketRequest;

class TicketController extends Controller
{
    protected TicketService $service;
    protected array $result;

    public function __construct(TicketService $ticketService)
    {
        $this->service = $ticketService;
        $this->result = ['status' => 200];
    }

    public function store(StoreTicketRequest $request): JsonResponse
    {
        try {
            $this->result['data'] = $this->service->storeTicketService($request->validated());
        }catch (Exception $e){
            $this->result =[
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json( $this->result,  $this->result['status']);
    }

    public function delete($id): JsonResponse
    {
        try {

            $result = $this->service->deleteTicketService($id);
        }catch (Exception $e){
            $this->result =[
                'status' => 500,
                'error' => $e->getMessage()
            ];

        }
        return response()->json( $result ,  $result['status'] ?? $this->result['status']);

    }


    public function update(UpdateTicketRequest $request, $id): JsonResponse
    {
        try {
            $result = $this->service->updateTicketService($request->validated(), $id);
        }catch (Exception $e){
            $this->result =[
                'status' => 500,
                'error' => $e->getMessage()
            ];

        }
        return response()->json( $result ,  $result['status'] ?? $this->result['status']);
    }
}
