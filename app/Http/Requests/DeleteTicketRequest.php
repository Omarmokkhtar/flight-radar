<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class DeleteTicketRequest extends FormRequest

{
    public function rules(): array
    {
        return [
            'id' => 'required|integer|exists:tickets,id',
        ];
    }


}
