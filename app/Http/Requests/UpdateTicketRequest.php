<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateTicketRequest extends FormRequest

{
    public function rules(): array
    {
        return [
            'seat_number' => 'required|integer|between:1,32',
        ];
    }
}
