<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTicketRequest extends FormRequest

{
    public function rules()

    {
        return [
            'departure_date' => 'required|date_format:Y-m-d H:i',
            'source' => 'required|max:50',
            'destination' => 'required|max:50',
            'passport_id' => 'required',
            'status' => 'nullable|boolean',
        ];
    }

}
